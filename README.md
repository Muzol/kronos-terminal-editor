## Kronos Text Editor

Kronos is a modern Terminal Text Editor. Build to be simple and custumized.

### Compile

`$gcc -O3 kronos.c -o kronos`

### Run

New File:

`$./kronos`

Open an existing File:

`$./kronos [filename]`